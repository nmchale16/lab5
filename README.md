# README #

git remote add origin https://nmchale16@bitbucket.org/nmchale16/lab5.git
git push -u origin master

### What is this repository for? ###

* This repository is for introductory purposes to allow us to get familar with repositories
* Version 1 for Lab 5
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* cd cpsc1021 && mkdir lab5 && cd lab5
* git init && ls -a
* git config --global user.name "Your Name"
* git config --global user.email "Your Email"
* git config --global core.editor vim
* g++ clock.cpp -o clock
* git status
* git add clock.cpp
* git commit
* edit program
* git add clock.cpp
* git commit -m "Fixed the time!"
* BitBucket instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Natalie McHale
* Nick Glyder